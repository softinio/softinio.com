+++
date = "2015-11-26T09:16:07-05:00"
Description = "About Salar Rahmanian"
keywords = ["Salar", "Rahmanian", "Salar Rahmanian"]
menu = "main"
slug = "salar-rahmanian"
title = "About Me"
+++

I , [Salar Rahmanian](https://www.softinio.com), am a software developer based in San Francisco Bay area, California.

I am married to my wonderful wife, [Leila Rahmanian](https://www.rahmanian.xyz/categories/leila-rahmanian/). We have two sons and a daughter, [Valentino Rahmanian](https://www.rahmanian.xyz/categories/valentino-rahmanian/) who was born in December 2013, [Caspian Rahmanian](https://www.rahmanian.xyz/categories/caspian-rahmanian/) who was born in December 2014 and [Persephone Rahmanian](https://www.rahmanian.xyz/) who was born in February 2017.

I have been developing software since the age of eleven. My current passion is
to develop distributed systems using Scala, AKKA and Kafka. For everything else I love coding in Python.

![Salar Rahmanian Family](/img/SalarRahmanianFamily.jpg)

