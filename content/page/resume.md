+++
date = "2018-04-16T09:16:07-05:00"
Description = "Salar Rahmanian Resume"
keywords = ["Salar", "Rahmanian", "Salar Rahmanian", "Resume", "CV"]
menu = "main"
slug = "salar-rahmanian-resume"
title = "Resume"
+++
# Salar Rahmanian #
## My Location ##
San Francisco Bay Area, California

## Profile ##
I have been developing software since the age of eleven. My current passion is to develop and architect distributed systems using Scala, AKKA and Kafka. For everything else I love coding in Python. I do consider myself a polyglot and have ability to pick up new languages and technology very fast.

## Domain Experience ##
- Building data ingestion pipelines
- Building High Volume Billing platforms (In particular interconnect billing for cell phone providers)
- Extensive domain knowledge of the job posting and matching and shift scheduling
- Building CMS and CRM systems
- Full Stack web application development

## Experience ##
### Senior Principal Engineer, Snag — 2016-present ###
I built a new data ingestion pipeline to ingest high volume of job postings received from multiple sources and partners. I built this using Scala, akka, akka-http, Play Framework, Kafka and Apache Nifi. I also setup a Kubernetes cluster running on AWS which was used for this new ingestion pipeline. I was responsible for introducing all these wonderful technologies to Snag.

Other projects I was involved in at Snag involved me creating micro services and REST api’s using Python, Flask.

### Senior Full Stack Software Developer, Trackmaven — 2015-2016 ###
Enhanced Trackmaven’s competitive intelligence platform by creating new services using Python, Flask, Django, Elasticsearch and PostgreSQL. Created a new UI Dashboard used by customers to track marketing campaign performance versus their competitor using Angular.

### Senior Software Developer, Accenture Federal Services — 2014-2015 ###
I worked on the modernization of the Dept of Veteran Affairs eHmp health care system. I was the technical lead of a team of 10 responsible for the development of the platforms sync system. This was a distributed system used to sync patient records across all hospitals. The main stack I used for development was Java, Spring, Dropwizard and Node.js.

### Chief Technology Officer, Healthy Choices At Work — 2012-2014 ###
I was a hands on CTO and Engineer at this startup. I architected and developed a custom CRM system for the business. This provided a full end to end solution which allowed customers to sign up for the service and manage their own accounts. On the business side it provided the means to run the business by providing the delivery scheduling of the orders and the billing and invoicing management of customers.

The stack used for this was Python, Django, Postgresql and Elasticsearch. For payments our system integrated with Stripe APIs. 

### Principal Software Developer, Incito Networks LLC — 2009-2013 ###
Worked as a freelance software developer developing custom apps for local small businesses and entrepreneurs. Worked on several software development projects which included me using: 
- Java, Spring
- Python, Django
- I developed two IOS apps for clients using Objective C 

### Principal Software Developer, Powermax Computing Ltd — 1998-2009 ###
If you have worked in the UK as an experienced software developer, you realize that the best jobs from all angles are contract / consultant positions. So during this period whilst living in London UK i worked for many well known companies on the biggest projects as a Principal Software Developer. In the early years my work solely involved coding using  C Programming on a Unix Platform (such as Solaris and AIX) with an Oracle Database backend (included Pro*C and PL/SQL). In the more later years my coding was mostly done using Java, Spring, Hibernate and Oracle again as the database backend.
The companies I worked for included: O2, IBM, Oracle, National Car Rental. 

### Education ###
King’s College, University of London — BEng(Hons) in Electronic and Electrical Engineering, Graduated in 1992

### References ###
Available on request

### Download my resume ###
[Click Here to download my resume](/media/Salar-Rahmanian-Resume.pdf)
